<?php

$HOME = "./";

function createDir($dir)
{
    global $HOME;

    $path = $HOME.$dir;
    if (!is_dir($path)) {
        return mkdir($path, 0777, true);
    } else {
        return true;
    }
}

function createFile($dir, $file, $content)
{
    global $HOME;

    $filepath = $dir."/".$file;
    if (file_put_contents($HOME.$filepath, $content)) {
        return $filepath;
    } else {
        return false;
    }
}

function delDir($dir)
{
    // https://stackoverflow.com/a/3349792
    
    global $HOME;

    $path = $HOME.$dir;

    if (is_dir($path)) {
        // create iterators
        $it = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);

        // recursively delete files and directories
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }

        // delete target directory
        rmdir($dir);
    }
}

function delFile($dir, $file)
{
    global $HOME;

    $filepath = $HOME.$file;
    if (unlink($filepath)) {
        return $filepath;
    } else {
        return false;
    }
}

function hashPassword($password)
{
    // por si acaso da tiempo
    return $password;
}
