<?php
include 'error_logger.php';
include 'utils.php';
include 'DBManager.php';

$postData = json_decode(file_get_contents('php://input'), true);
$email = $postData['email'];
$paths = $postData['paths'];

if ($email == null || $paths == null) {
    echo "false";
    exit();
}

$dir = $email;

$dbManager = new DBManager();
foreach($paths as $path) {
    if ($dbManager->delImage($email, $path) > 0) {
        delFile($dir, $path);
    }
}

exit();
