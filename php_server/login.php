<?php
// include 'error_logger.php';
include 'utils.php';
include 'DBManager.php';

$postData = json_decode(file_get_contents('php://input'), true);
$email = $postData['email'];
$password = $postData['password'];

if ($email == null || $password == null) {
    echo "false";
    exit();
}

$passwordHash = hashPassword($password);

$dbManager = new DBManager();
$dbPasswordHash = $dbManager->getUserHash($email);

if ($dbPasswordHash === $passwordHash) {
    $userData = $dbManager->getUser($email);
    echo json_encode($userData);
    exit();
} else {
    echo "false";
    exit();
}
