<?php
//include 'error_logger.php';
include 'utils.php';
include 'firebase_message.php';
include 'DBManager.php';

$postData = json_decode(file_get_contents('php://input'), true);
$email = $postData['email'];
$imgBin = $postData['image'];
$imgName = $postData['name'];
$firebaseToken = $postData['token'];

if ($email == null || $imgBin == null || $imgName == null) {
    echo "false";
    exit();
}

$dir = $email;
$file = $imgName;
$content = $imgBin;

$dbManager = new DBManager();
if ($dbManager->getUser($email)) {
    if (createDir($dir)) {
        if ($path = createFile($dir, $file, base64_decode($content))) {
            $dbManager->addImage($email, $path);
            if ($dbManager->hasImage($email, $path)) {
                if ($firebaseToken != null) {
                    send_message($firebaseToken, $imgName);
                }
                echo $path;
                exit();
            }
        }
    }
}

echo "false";
exit();
