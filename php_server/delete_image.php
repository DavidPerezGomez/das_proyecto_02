<?php
//include 'error_logger.php';
include 'utils.php';
include 'DBManager.php';

$postData = json_decode(file_get_contents('php://input'), true);
$email = $postData['email'];
$path = $postData['path'];

if ($email == null || $path == null) {
    echo "false";
    exit();
}

$dir = $email;

$dbManager = new DBManager();
if ($dbManager->delImage($email, $path) > 0) {
    delFile($dir, $path);
    echo $path;
    exit();
}

echo "false";
exit();
