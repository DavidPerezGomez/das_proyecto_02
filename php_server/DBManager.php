<?php

class DBManager
{
    public static $DB_SERVER = "127.0.0.1"; # la dirección del servidor
    public static $DB_USER = "Xdperez067"; # el usuario para esa base de datos
    public static $DB_PASS = "DYz03Tjrtj"; # la clave para ese usuario
    public static $DB_DATABASE = "Xdperez067_MediaManager"; # la base de datos a la que hay que conectarse

    public static $TABLE_USER = "user";
    public static $TABLE_USER_EMAIL = "email";
    public static $TABLE_USER_NAME = "name";
    public static $TABLE_USER_PASSWORD = "password_hash";

    public static $TABLE_IMAGE = "image";
    public static $TABLE_IMAGE_EMAIL = "email";
    public static $TABLE_IMAGE_PATH = "path";

    private $connection;

    private function getConnection()
    {
        if ($this->connection == null) {
            $this->connection = mysqli_connect(
                DBManager::$DB_SERVER,
                DBManager::$DB_USER,
                DBManager::$DB_PASS,
                DBManager::$DB_DATABASE
            );

            # Comprobamos conexión
            if (mysqli_connect_errno($this->connection)) {
                // echo 'Error de conexion: ' . mysqli_connect_error();
                return null;
            }
        }
        return $this->connection;
    }

    public function addUser($email, $name, $passwordHash)
    {
        $sql = "INSERT INTO ".DBManager::$TABLE_USER
                ." ("
                .DBManager::$TABLE_USER_EMAIL.", "
                .DBManager::$TABLE_USER_NAME.", "
                .DBManager::$TABLE_USER_PASSWORD
                .")"
                ." VALUES ("
                ."'".$email."', '".$name."', '".$passwordHash."'"
                .")";

        $res = mysqli_query(
            $this->getConnection(),
            $sql
        );

        # Comprobar si se ha ejecutado correctamente
        if (!$res) {
            // echo 'Ha ocurrido algún error: ' . mysqli_error($this->getConnection());
        }

        return $res;
    }

    public function getUser($email)
    {
        $sql = "SELECT "
                .DBManager::$TABLE_USER_NAME
                ." FROM ".DBManager::$TABLE_USER
                ." WHERE ".DBManager::$TABLE_USER_EMAIL."='".$email."'";

        $res = mysqli_query(
            $this->getConnection(),
            $sql
        );

        # Comprobar si se ha ejecutado correctamente
        if (!$res) {
            // echo 'Ha ocurrido algún error: ' . mysqli_error($this->getConnection());
            return null;
        }

        $userData = [];
        while (($row = mysqli_fetch_row($res)) != null) {
            $userData = [
                DBManager::$TABLE_USER_EMAIL => $email,
                DBManager::$TABLE_USER_NAME => $row[0]
            ];
        }

        return $userData;
    }

    public function delUser($email)
    {
        $sql = "DELETE FROM ".DBManager::$TABLE_USER
                ." WHERE ".DBManager::$TABLE_USER_EMAIL."='".$email."'";

        $res = mysqli_query(
            $this->getConnection(),
            $sql
        );

        # Comprobar si se ha ejecutado correctamente
        if (!$res) {
            // echo 'Ha ocurrido algún error: ' . mysqli_error($this->getConnection());
        }

        return $res;
    }

    public function getUserHash($email)
    {
        $sql = "SELECT "
                .DBManager::$TABLE_USER_PASSWORD
                ." FROM ".DBManager::$TABLE_USER
                ." WHERE ".DBManager::$TABLE_USER_EMAIL."='".$email."'";

        $res = mysqli_query(
            $this->getConnection(),
            $sql
        );

        # Comprobar si se ha ejecutado correctamente
        if (!$res) {
            // echo 'Ha ocurrido algún error: ' . mysqli_error($this->getConnection());
            return null;
        }

        $hash = [];
        while (($row = mysqli_fetch_row($res)) != null) {
            $hash = $row[0];
        }

        return $hash;
    }

    public function addImage($email, $path)
    {
        $sql = "INSERT INTO ".DBManager::$TABLE_IMAGE
                ." ("
                .DBManager::$TABLE_IMAGE_EMAIL.", "
                .DBManager::$TABLE_IMAGE_PATH
                .")"
                ." VALUES ("
                ."'".$email."', '".$path."'"
                .")";

        $res = mysqli_query(
            $this->getConnection(),
            $sql
        );

        # Comprobar si se ha ejecutado correctamente
        if (!$res) {
            // echo 'Ha ocurrido algún error: ' . mysqli_error($this->getConnection());
        }

        return $res;
    }

    public function hasImage($email, $path)
    {
        $sql = "SELECT * FROM ".DBManager::$TABLE_IMAGE
                ." WHERE ".DBManager::$TABLE_IMAGE_EMAIL."='".$email."'"
                ." AND ".DBManager::$TABLE_IMAGE_PATH."='".$path."'";

        $res = mysqli_query(
            $this->getConnection(),
            $sql
        );

        # Comprobar si se ha ejecutado correctamente
        if (!$res) {
            // echo 'Ha ocurrido algún error: ' . mysqli_error($this->getConnection());
            return false;
        }

        return (mysqli_fetch_row($res) != null);
    }

    public function delImage($email, $path)
    {
        $sql = "DELETE FROM ".DBManager::$TABLE_IMAGE
                ." WHERE ".DBManager::$TABLE_IMAGE_EMAIL."='".$email."'"
                ." AND ".DBManager::$TABLE_IMAGE_PATH."='".$path."'";

        $res = mysqli_query(
            $this->getConnection(),
            $sql
        );

        # Comprobar si se ha ejecutado correctamente
        if (!$res) {
            // echo 'Ha ocurrido algún error: ' . mysqli_error($this->getConnection());
            return -1;
        }

        return mysqli_affected_rows($this->getConnection());
    }

    public function getImagesPath($email)
    {
        $sql = "SELECT "
                .DBManager::$TABLE_IMAGE_PATH
                ." FROM ".DBManager::$TABLE_IMAGE
                ." WHERE ".DBManager::$TABLE_IMAGE_EMAIL."='".$email."'";

        $res = mysqli_query(
            $this->getConnection(),
            $sql
        );

        # Comprobar si se ha ejecutado correctamente
        if (!$res) {
            // echo 'Ha ocurrido algún error: ' . mysqli_error($this->getConnection());
            return null;
        }

        $paths = [];
        while (($row = mysqli_fetch_row($res)) != null) {
            $paths[] = $row[0];
        }

        return $paths;
    }
}
