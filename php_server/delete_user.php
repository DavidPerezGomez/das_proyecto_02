<?php
// include 'error_logger.php';
include 'utils.php';
include 'DBManager.php';

$postData = json_decode(file_get_contents('php://input'), true);
$email = $postData['email'];

if ($email == null) {
    echo "false";
    exit();
}

$dbManager = new DBManager();
if ($dbManager->getUser($email)) {
    if ($dbManager->delUser($email)) {
        delDir($email);
        echo "true";
        exit();
    }
}

echo "false";
exit();
