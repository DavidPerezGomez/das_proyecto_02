<?php
// include 'error_logger.php';
include 'utils.php';
include 'DBManager.php';

$postData = json_decode(file_get_contents('php://input'), true);
$email = $postData['email'];

if ($email == null) {
    echo "false";
    exit();
}

$dbManager = new DBManager();
if ($paths = $dbManager->getImagesPath($email)) {
    $res = [];
    foreach ($paths as $index => $path) {
        $res[] = [
            "name" => basename($path),
            "path" => $path
            //"image" => file_get_contents($path)
        ];
    }

    echo json_encode($res);
    exit();
}

echo "false";
exit();
