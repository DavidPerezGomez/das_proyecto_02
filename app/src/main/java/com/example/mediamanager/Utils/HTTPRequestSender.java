package com.example.mediamanager.Utils;

import android.net.Uri;
import android.os.AsyncTask;
import com.example.mediamanager.MediaManagerApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;

/**
 * Class to perform all the HTTP request to the server
 */
public class HTTPRequestSender extends AsyncTask<Object, Void, Boolean> {

    private static final String ADDRESS = "https://134.209.235.115/dperez067/WEB/";

    private static final int LOG_IN_CODE = 0;
    private static final int REGISTER_CODE = 1;
    private static final int DEL_USER_CODE = 2;
    private static final int ADD_IMG_CODE = 3;
    private static final int DEL_IMG_CODE = 4;
    private static final int DEL_IMGS_CODE = 5;
    private static final int GET_IMGS_CODE = 6;

    private HTTPListener listener;
    private int callbackCode = -1;
    private String response = "";

    public interface HTTPListener {

        /**
         * Called after receiving response from a login
         * request to the server.
         *
         * @param result Result of the request. If successful, it contains
         *               the user's information. It's null otherwise.
         */
        default void onLogInResponse(JSONObject result) {}

        /**
         * Called after receiving response from a register
         * request to the server.
         *
         * @param result Result of the request. If successful, it contains
         *               the user's information. It's null otherwise.
         */
        default void onRegisterResponse(JSONObject result) {}

        /**
         * Called after receiving response from a request to
         * send an image to the server.
         *
         * @param result Result of the request. If successful, it contains
         *               the path where the image was saved. It's null otherwise.
         */
        default void onUploadImageResponse(String result) {}

        /**
         * Called after receiving response from a request to
         * get all of a user's images from the server.
         *
         * @param result Result of the request. If successful, it contains
         *               a list of the images' names and urls. It's null otherwise.
         */
        default void onGetImagesResponse(JSONArray result) {}

        /**
         * Called after receiving response from a request to
         * delete and image from the server.
         *
         * @param result Result of the request. If successful, it contains
         *               the path of the deleted image. It's null otherwise.
         */
        default void onDeleteImgResponse(String result) {}
    }

    public HTTPRequestSender(HTTPListener listener) {
        this.listener = listener;
    }

    /**
     * Performs the login request to the server.
     *
     * @param email    Login email
     * @param password Login password
     */
    public void logIn(String email, String password) {
        String file = "login.php";
        try {
            JSONObject data = new JSONObject();
            data.put("email", email);
            data.put("password", password);
            this.execute(LOG_IN_CODE, file, data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Performs the register request to the server.
     *
     * @param email    User email
     * @param name     User name
     * @param password User password
     */
    public void register(String email, String name, String password) {
        String file = "sign_in.php";
        try {
            JSONObject data = new JSONObject();
            data.put("email", email);
            data.put("name", name);
            data.put("password", password);
            this.execute(REGISTER_CODE, file, data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Performs a request to upload an image to the server.
     *
     * @param email         User email
     * @param imageUri      Image uri
     * @param imageName     Image name
     * @param firebaseToken Device's firebase token
     */
    public void uploadImage(String email, Uri imageUri, String imageName, String firebaseToken) {
        // http://programmerguru.com/android-tutorial/how-to-upload-image-to-php-server/

        GenericAsyncTask imageLoader = new GenericAsyncTask(() -> {
            // load the image asynchronously
            return Utils.getImageBinary(imageUri);
        }, imageData -> {
            // perform the HTTP request
            try {
                String file = "add_image.php";

                JSONObject data = new JSONObject();
                data.put("email", email);
                data.put("image", imageData);
                data.put("name", imageName);
                data.put("token", firebaseToken);

                this.execute(ADD_IMG_CODE, file, data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        imageLoader.execute();
    }

    /**
     * Performs a request to get all of a user's images stored
     * on the server.
     *
     * @param email User email
     */
    public void getImages(String email) {
        String file = "get_images.php";
        try {
            JSONObject data = new JSONObject();
            data.put("email", email);
            this.execute(GET_IMGS_CODE, file, data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Performs a request to delete a user's image from the server.
     *
     * @param email User email
     * @param path  Path to the image
     */
    public void deleteImage(String email, String path) {
        String file = "delete_image.php";
        if (path.startsWith(ADDRESS)) {
            path = path.substring(ADDRESS.length());
        }
        while (path.startsWith("/")) {
            path = path.substring(1);
        }
        try {
            JSONObject data = new JSONObject();
            data.put("email", email);
            data.put("path", path);
            this.execute(DEL_IMG_CODE, file, data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Performs a request to delete several of a user's images
     * from the server.
     *
     * @param email User email
     * @param paths Paths to the images
     */
    public void deleteImages(String email, String[] paths) {
        String file = "delete_images.php";

        try {
            JSONObject data = new JSONObject();
            data.put("email", email);

            JSONArray pathsJSON = new JSONArray();
            for (int i = 0; i < paths.length; i++) {
                String path = paths[i];
                if (path.startsWith(ADDRESS)) {
                    path = path.substring(ADDRESS.length());
                }
                while (path.startsWith("/")) {
                    path = path.substring(1);
                }
                pathsJSON.put(path);
            }
            data.put("paths", pathsJSON);
            this.execute(DEL_IMGS_CODE, file, data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Boolean doInBackground(Object... objects) {
        int callbackCode = (int) objects[0];
        String file = (String) objects[1];
        String data = (String) objects[2];

        try {
            String address = ADDRESS + file;
            HttpURLConnection urlConnection = GeneradorConexionesSeguras.getInstance().crearConexionSegura(
                    MediaManagerApplication.getAppContext(),
                    address);
            urlConnection.setConnectTimeout(5000);
            urlConnection.setReadTimeout(5000);
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");

            PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
            out.print(data);
            out.close();

            int statusCode = urlConnection.getResponseCode();
            if (statusCode == 200) {
                BufferedInputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String line;
                StringBuilder result = new StringBuilder();
                while ((line = bufferedReader.readLine()) != null) {
                    result.append(line);
                }

                inputStream.close();

                this.callbackCode = callbackCode;
                this.response = result.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        handleResult(this.callbackCode, this.response);
    }

    private void handleResult(int code, String response) {
        JSONObject result = null;
        JSONArray resultList = null;
        switch (code) {
            case LOG_IN_CODE:
                if (!response.isEmpty() && !response.equals("false")) {
                    try {
                        result = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                this.listener.onLogInResponse(result);
                break;
            case REGISTER_CODE:
                if (!response.isEmpty() && !response.equals("false")) {
                    try {
                        result = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                this.listener.onRegisterResponse(result);
                break;
            case DEL_USER_CODE:
                break;
            case ADD_IMG_CODE:
                if (!response.isEmpty() && !response.equals("false")) {
                    this.listener.onUploadImageResponse(response);
                } else {
                    this.listener.onUploadImageResponse(null);
                }
                break;
            case DEL_IMG_CODE:
                if (!response.isEmpty() && !response.equals("false")) {
                    this.listener.onDeleteImgResponse(ADDRESS + response);
                } else {
                    this.listener.onDeleteImgResponse(null);
                }
                break;
            case DEL_IMGS_CODE:
                break;
            case GET_IMGS_CODE:
                if (!response.isEmpty() && !response.equals("false")) {
                    try {
                        resultList = new JSONArray(response);
                        for (int i = 0; i < resultList.length(); i++) {
                            JSONObject imageInfo = resultList.getJSONObject(i);
                            String path = imageInfo.getString("path");
                            imageInfo.put("path", ADDRESS + path);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                this.listener.onGetImagesResponse(resultList);
                break;
            default:
                break;
        }
    }

}
