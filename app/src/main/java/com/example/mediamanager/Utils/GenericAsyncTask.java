package com.example.mediamanager.Utils;

import android.os.AsyncTask;

/**
 * Class to perform custom functionality asynchronously.
 */
public class GenericAsyncTask extends AsyncTask<Void, Void, String> {

    public interface GenericTaskListener {

        String doInBackground();
    }

    public interface GenericTaskFinishedListener {

        void taskFinished(String code);
    }

    private GenericTaskListener listener;
    private GenericTaskFinishedListener taskFinishedListener = null;

    public GenericAsyncTask(GenericTaskListener listener, GenericTaskFinishedListener taskFinishedListener) {
        this.listener = listener;
        this.taskFinishedListener = taskFinishedListener;
    }

    public GenericAsyncTask(GenericTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... voids) {
        return listener.doInBackground();
    }

    @Override
    protected void onPostExecute(String s) {
        System.out.println("done...");

        if (taskFinishedListener != null) {
            taskFinishedListener.taskFinished(s);
        }
    }
}
