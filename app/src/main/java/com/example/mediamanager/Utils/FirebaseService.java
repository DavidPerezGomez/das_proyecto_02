package com.example.mediamanager.Utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import com.example.mediamanager.MediaManagerApplication;
import com.example.mediamanager.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Class to manage the Firebase Cloud Messaging service
 */
public class FirebaseService extends FirebaseMessagingService {

    public FirebaseService() {
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        // save the token on AccountManager
        AccountManager.signInFirebase(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();
        if (data.size() > 0) {
            showNotification(data.get("imageName"));
        }
    }

    /**
     * Shows a notification indicating that the image has been received.
     *
     * @param imageName Name of the image.
     */
    private void showNotification(String imageName) {
        NotificationManager elManager = (NotificationManager) getSystemService(MediaManagerApplication.getAppContext().NOTIFICATION_SERVICE);
        NotificationCompat.Builder elBuilder = new NotificationCompat.Builder(this, "IdCanal");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel elCanal = new NotificationChannel("IdCanal", "NombreCanal", NotificationManager.IMPORTANCE_DEFAULT);
            elManager.createNotificationChannel(elCanal);
        }

        elBuilder.setSmallIcon(android.R.drawable.star_on)
                .setContentTitle(getString(R.string.notification_fcm_image_received_title))
                .setContentText(String.format(getString(R.string.notification_fcm_image_received_text), imageName))
                .setVibrate(new long[]{0, 250, 250, 250})
                .setAutoCancel(true);

        elManager.notify(0, elBuilder.build());
    }
}
