package com.example.mediamanager.Utils;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.api.services.drive.DriveScopes;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

/**
 * Class to check and request permission from the user.
 */
public class PermissionManager {

    public static final int GOOGLE_SIGN_IN_CODE = 0;
    public static final int DRIVE_PERMISSION_REQUEST_CODE = 1;
    public static final int STORAGE_AND_CAMERA_PERMISSION_REQUEST_CODE = 3;

    /**
     * Check whether the application has permission to read/write the
     * external storage and to make use of the camera. Request those permissions
     * if necessary.
     *
     * @param context Application context
     * @return True if the permissions are granted, False otherwise. The
     * return value doesn't take into account the users response the
     * request.
     */
    public static boolean checkStorageAndCameraPermissions(AppCompatActivity context) {
        // check for WRITE_EXTERNAL_STORAGE permission
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) !=
                               PackageManager.PERMISSION_GRANTED) {
            // ask for permission
            ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, STORAGE_AND_CAMERA_PERMISSION_REQUEST_CODE);

            return false;
        } else {
            // granted
            return true;
        }
    }

    /**
     * Checks whether the user is logged in with Google. Asks to log in
     * with Google if necessary.
     *
     * @param context Application context
     */
    public static boolean checkGoogleAccount(AppCompatActivity context) {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(context);
        if (account == null || account.getEmail() == null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                              .requestEmail()
                                              .build();
            GoogleSignInClient cliente = GoogleSignIn.getClient(context, gso);
            Intent intentIdentif = cliente.getSignInIntent();
            context.startActivityForResult(intentIdentif, GOOGLE_SIGN_IN_CODE);

            return false;
        } else {
            AccountManager.signInGoogle(account);
            return true;
        }
    }

    /**
     * Checks whether the application has permission to read/write the
     * user's Google Drive files. Requests permission if necessary.
     *
     * @param context Application context
     * @return True if the permission is granted, False otherwise. The
     * return value doesn't take into account the users response the
     * request.
     */
    public static boolean checkDrivePermission(AppCompatActivity context) {
        if (!GoogleSignIn.hasPermissions(AccountManager.getGoogleAccount(),
                new Scope(DriveScopes.DRIVE), new Scope(DriveScopes.DRIVE_FILE))) {
            GoogleSignIn.requestPermissions(
                    context,
                    DRIVE_PERMISSION_REQUEST_CODE,
                    AccountManager.getGoogleAccount(),
                    new Scope(DriveScopes.DRIVE),
                    new Scope(DriveScopes.DRIVE_FILE));
            return false;
        }
        return true;
    }

    /**
     * Reads and stored the device's firebase token.
     */
    public static void signInFirebase() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String token = task.getResult().getToken();
                        AccountManager.signInFirebase(token);
                    }
                });
    }
}
