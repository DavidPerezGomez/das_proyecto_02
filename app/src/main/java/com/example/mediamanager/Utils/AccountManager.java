package com.example.mediamanager.Utils;

import com.example.mediamanager.MediaManagerApplication;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;

import java.util.Collections;

/**
 * Static class to manage everything related to user authentication and information.
 */
public class AccountManager {

    /**
     * Basic account with user's name and email
     */
    private static Account account;

    /**
     * Account result of signing in with Google
     */
    private static GoogleSignInAccount googleAccount;

    /**
     * Token assigned by Firebase Cloud Messaging to the device
     */
    private static String firebaseToken;

    /**
     * Helper class to make use of Google Drive's API
     */
    private static DriveServiceHelper driveServiceHelper;

    public static Account getAccount() {
        return account;
    }

    public static GoogleSignInAccount getGoogleAccount() {
        return googleAccount;
    }

    public static String getFirebaseToken() {
        return firebaseToken;
    }

    public static DriveServiceHelper getDriveServiceHelper() {
        if (driveServiceHelper == null) {
            GoogleSignInAccount account = AccountManager.getGoogleAccount();
            if (account == null) {
                return null;
            }

            GoogleAccountCredential credential =
                    GoogleAccountCredential.usingOAuth2(
                            MediaManagerApplication.getAppContext(),
                            Collections.singleton(DriveScopes.DRIVE_FILE));
            credential.setSelectedAccount(account.getAccount());

            Drive googleDriveService =
                    new Drive.Builder(
                            AndroidHttp.newCompatibleTransport(),
                            new GsonFactory(),
                            credential)
                            .setApplicationName("MediaManager")
                            .build();

            driveServiceHelper = new DriveServiceHelper(googleDriveService);
        }

        return driveServiceHelper;
    }

    public static void signIn(String email, String name) {
        account = new Account(email, name);
    }

    public static void signOut() {
        account = null;
    }

    public static void signInGoogle(GoogleSignInAccount pGoogleAccount) {
        googleAccount = pGoogleAccount;
    }

    public static void signOutGoogle() {
        // sign out from Google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                          .requestEmail()
                                          .build();
        GoogleSignInClient cliente = GoogleSignIn.getClient(MediaManagerApplication.getAppContext(), gso);
        cliente.signOut();

        // unset Google-related variables
        driveServiceHelper = null;
        googleAccount = null;
    }

    public static void signInFirebase(String token) {
        firebaseToken = token;
    }

    public static class Account {

        private String email;
        private String name;

        public Account(String email, String name) {
            this.email = email;
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public String getName() {
            return name;
        }
    }

}
