package com.example.mediamanager.Utils;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import com.example.mediamanager.MediaManagerApplication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Class to perform functionality related to the device's media
 * and storage.
 */
public class MediaManager {

    @SuppressLint("StaticFieldLeak")
    private final static Context context = MediaManagerApplication.getAppContext();

    /**
     * Obtains a list of all the public images stored in
     * the device containing their ids, names and uris.
     *
     * @return JSON-formatted string with the images information.
     */
    public static String getImages() {
        // create query data
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] columns = {MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DISPLAY_NAME};
        String condition = null;
        String[] args = null;
        String order = MediaStore.Images.Media.DATE_ADDED;

        // perform query
        Cursor cursor = context.getContentResolver().query(
                uri,
                columns,
                condition,
                args,
                order);

        // initialize result JSON
        JSONArray jsonResult = new JSONArray();

        if (cursor != null) {
            // iterate query result
            while (cursor.moveToNext()) {
                // obtain data
                long id = cursor.getLong(0);
                String name = cursor.getString(1);
                Uri imgUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);

                // insert data into result JSON
                JSONObject jsonImage = new JSONObject();
                try {
                    jsonImage.put("id", id);
                    jsonImage.put("name", name);
                    jsonImage.put("uri", imgUri);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonResult.put(jsonImage);
            }

            cursor.close();
        }

        return jsonResult.toString();
    }

    /**
     * Deletes public images from the device's storage.
     *
     * @param imgUris Uris of the images to delete
     * @return The number of images deleted.
     */
    public static int deleteImages(Uri[] imgUris) {
        int res = 0;
        for (Uri imgUri : imgUris) {
            res += deleteImage(imgUri);
        }
        return res;
    }

    /**
     * Deletes a public image from the device's storage.
     *
     * @param imgUri Uri of the image to delete
     * @return The number of images deleted (1 or 0).
     */
    public static int deleteImage(Uri imgUri) {
        // create query data
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String condition = MediaStore.Images.Media._ID + " = ?";
        String[] args = {Long.toString(ContentUris.parseId(imgUri))};

        // perform query
        return context.getContentResolver().delete(
                uri,
                condition,
                args);

    }

    /**
     * Returns a bitmap of the specified image.
     *
     * @param uri Uri of the image
     * @return Bitmap of the image.
     */
    public static Bitmap getBitmap(Uri uri) {
        return getBitmap(uri, -1, -1);
    }

    /**
     * Returns a bitmap of the specified image scaled down (never up)
     * to try to match the give dimensions while maintaining it's
     * original aspect ration.
     *
     * @param uri Uri of the image
     * @param width Target width
     * @param height Target height
     * @return Bitmap of the image.
     */
    public static Bitmap getBitmap(Uri uri, float width, float height) {
        try {
            // https://stackoverflow.com/a/6228188
            try {
                // load the image's bounds
                BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
                onlyBoundsOptions.inJustDecodeBounds = true;

                InputStream input = context.getContentResolver().openInputStream(uri);
                BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
                input.close();

                if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
                    return null;
                }

                float originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;
                float targetSize = (height > width) ? height : width;

                // compare original and target size to determine the desired ratio
                double ratio = targetSize > 0 && (originalSize > targetSize) ? (originalSize / targetSize) : 1.0;

                // load the Bitmap
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);

                input = context.getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
                input.close();

                return bitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } catch (OutOfMemoryError e) {
            return null;
        }
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0)
            return 1;
        else
            return k;
    }

}
