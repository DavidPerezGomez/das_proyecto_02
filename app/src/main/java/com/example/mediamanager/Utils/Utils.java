package com.example.mediamanager.Utils;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Utils {

    /**
     * Obtains the base 64 String with the contents of
     * and image.
     *
     * @param uri Uri of the image
     * @return String with the contents of the image.
     */
    public static String getImageBinary(Uri uri) {
        String imageData = "";
        try {
            // get the image bitmap
            Bitmap image = MediaManager.getBitmap(uri);

            // transform the bitmap into bytes
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 60, stream);
            byte[] byte_arr = stream.toByteArray();

            // transform the bytes into a String
            imageData = Base64.encodeToString(byte_arr, 0);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return imageData;
    }

}
