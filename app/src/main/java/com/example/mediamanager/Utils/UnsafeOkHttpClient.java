package com.example.mediamanager.Utils;

import com.example.mediamanager.MediaManagerApplication;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;

import javax.net.ssl.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;

/**
 * https://github.com/futurestudio/android-tutorials-glide/blob/master/app/src/main/java/io/futurestud/tutorials/glide/okhttp/UnsafeOkHttpClient.java
 *
 * Created by norman on 2/23/15.
 */
public class UnsafeOkHttpClient {

    public static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            X509TrustManager trustAllCerts = new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    CertificateFactory certFactory = null;
                    try {
                        certFactory = CertificateFactory.getInstance("X.509");
                        InputStream inputStream = new BufferedInputStream(MediaManagerApplication.getAppContext().getAssets().open("das-cert.crt"));

                        X509Certificate cert = (X509Certificate) certFactory
                                                                         .generateCertificate(inputStream);
                        return new java.security.cert.X509Certificate[]{cert};
                    } catch (CertificateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            };

            final TrustManager[] trustAllCertsList = new TrustManager[]{trustAllCerts};

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCertsList, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts);
            builder.protocols(Arrays.asList(Protocol.HTTP_1_1));
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
