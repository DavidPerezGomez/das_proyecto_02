package com.example.mediamanager.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import com.example.mediamanager.R;

public class UploadDialog extends DialogFragment {

    public interface UploadDialogListener {

        void onClickUploadServer();

        void onClickUploadDrive();
    }

    private UploadDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        this.listener = (UploadDialogListener) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_upload_image_title));
        String[] options = {getString(R.string.dialog_upload_image_option_server),
                getString(R.string.dialog_upload_image_option_drive)};
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    listener.onClickUploadServer();
                    break;
                case 1:
                    listener.onClickUploadDrive();
                    break;
            }
        });

        return builder.create();
    }
}
