package com.example.mediamanager.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import com.example.mediamanager.R;

public class GoogleSignInFailDialog extends DialogFragment {

    public interface GoogleSignInFailDialogListener {

        void onClickRetrySignIn();

        default void onClickCancelSignIn() {}
    }

    private GoogleSignInFailDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        this.listener = (GoogleSignInFailDialogListener) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_google_sign_in_fail_title));

        builder.setPositiveButton(getString(R.string.dialog_google_sign_in_fail_retry), (dialog, which) -> listener.onClickRetrySignIn());

        builder.setNeutralButton(getString(R.string.dialog_google_sign_in_fail_cancel), (dialog, which) -> listener.onClickCancelSignIn());

        return builder.create();
    }

}
