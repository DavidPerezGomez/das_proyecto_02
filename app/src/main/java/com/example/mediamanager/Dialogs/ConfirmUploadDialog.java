package com.example.mediamanager.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import com.example.mediamanager.R;

public class ConfirmUploadDialog extends DialogFragment {

    public static final int SERVER = 0;
    public static final int DRIVE = 1;

    public interface ConfirmUploadDialogListener {

        void onConfirmUploadServer();

        void onConfirmUploadDrive();

        default void onClickCancelUpload() {}
    }

    private ConfirmUploadDialogListener listener;
    private int destination = -1;

    public void setDestination(int destination) {
        this.destination = destination;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (this.destination > -1) {
            super.onCreateDialog(savedInstanceState);

            this.listener = (ConfirmUploadDialogListener) getActivity();

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            switch (this.destination) {
                case SERVER: //Server
                    builder.setTitle(getString(R.string.dialog_confirm_upload_server_title));
                    builder.setMessage(getString(R.string.dialog_confirm_upload_server_text));

                    builder.setPositiveButton(getString(R.string.dialog_confirm_upload_server_continue), (dialog, which) -> listener.onConfirmUploadServer());

                    builder.setNeutralButton(getString(R.string.dialog_confirm_upload_server_cancel), (dialog, which) -> listener.onClickCancelUpload());
                    break;
                case DRIVE: //Server
                    builder.setTitle(getString(R.string.dialog_confirm_upload_drive_title));
                    builder.setMessage(getString(R.string.dialog_confirm_upload_drive_text));

                    builder.setPositiveButton(getString(R.string.dialog_confirm_upload_drive_continue), (dialog, which) -> listener.onConfirmUploadDrive());

                    builder.setNeutralButton(getString(R.string.dialog_confirm_upload_drive_cancel), (dialog, which) -> listener.onClickCancelUpload());
                    break;
            }

            return builder.create();
        } else {
            this.dismiss();
            return null;
        }
    }
}
