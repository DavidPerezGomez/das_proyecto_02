package com.example.mediamanager.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import com.example.mediamanager.R;

public class DeleteDialog extends DialogFragment {

    public interface DeleteDialogListener {

        void onClickDelete();

        default void onClickCancelDelete() {}
    }

    private DeleteDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        this.listener = (DeleteDialogListener) getActivity();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.dialog_delete_image_title));
        builder.setMessage(getString(R.string.dialog_delete_image_text));

        builder.setPositiveButton(getString(R.string.dialog_delete_image_continue), (dialog, which) -> listener.onClickDelete());

        builder.setNeutralButton(getString(R.string.dialog_delete_image_cancel), (dialog, which) -> listener.onClickCancelDelete());

        return builder.create();
    }
}
