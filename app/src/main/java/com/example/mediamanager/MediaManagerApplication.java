package com.example.mediamanager;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

public class MediaManagerApplication extends Application {

    @SuppressLint("StaticFieldLeak")
    private static Context context;

    public void onCreate() {
        super.onCreate();
        MediaManagerApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MediaManagerApplication.context;
    }
}
