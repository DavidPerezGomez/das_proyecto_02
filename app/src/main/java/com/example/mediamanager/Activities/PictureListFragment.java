package com.example.mediamanager.Activities;

import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import com.example.mediamanager.Activities.Support.GenericListFragment;
import com.example.mediamanager.Activities.Support.GenericSelectionAdapter;
import com.example.mediamanager.Activities.Support.PictureListAdapter;

import java.util.ArrayList;

public class PictureListFragment extends GenericListFragment<PictureListAdapter> implements GenericSelectionAdapter.SelectionAdapterListener {

    public void setData(ArrayList<Uri> uris, ArrayList<String> names) {
        super.setData();

        // create adapter
        PictureListAdapter adapter = new PictureListAdapter(this, uris, names);
        list.setAdapter(adapter);

        // create layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        list.setLayoutManager(linearLayoutManager);
    }

    /**
     * @return A list containing all the uris selected from the list.
     */
    public Uri[] getSelectedUris() {
        return ((PictureListAdapter) this.list.getAdapter()).getSelectedUris();
    }

    /**
     * Removes the selected items from the list.
     */
    public void deleteSelected() {
        ((PictureListAdapter) this.list.getAdapter()).deleteSelectedItems();
    }
}
