package com.example.mediamanager.Activities.Support;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mediamanager.R;

public class PictureListViewHolder extends RecyclerView.ViewHolder {

    public interface PictureListViewHolderListener {

        void onItemClick(int index);

        void onItemLongClick(int index);
    }

    private PictureListViewHolderListener listener = null;
    private int index = -1;

    public ImageView imageView;
    public TextView textView;

    public PictureListViewHolder(@NonNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.itemListImage);

        textView = itemView.findViewById(R.id.itemListText);

        View.OnClickListener clickListener = (View view) -> {
            if (this.listener != null) {
                this.listener.onItemClick(this.index);
            }
        };

        View.OnLongClickListener longClickListener = (View view) -> {
            if (this.listener != null) {
                this.listener.onItemLongClick(this.index);
            }

            return true;
        };

        textView.setOnClickListener(clickListener);
        imageView.setOnClickListener(clickListener);

        textView.setOnLongClickListener(longClickListener);
        imageView.setOnLongClickListener(longClickListener);
    }

    public void setListener(PictureListViewHolderListener listener, int index) {
        this.listener = listener;
        this.index = index;
    }

}
