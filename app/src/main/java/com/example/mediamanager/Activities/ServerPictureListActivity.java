package com.example.mediamanager.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.mediamanager.*;
import com.example.mediamanager.Activities.Support.GenericListFragment;
import com.example.mediamanager.Dialogs.DeleteDialog;
import com.example.mediamanager.Utils.AccountManager;
import com.example.mediamanager.Utils.HTTPRequestSender;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ServerPictureListActivity extends LoaderActivity implements GenericListFragment.ListFragmentListener, DeleteDialog.DeleteDialogListener, HTTPRequestSender.HTTPListener {

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_list);

        Button buttonBack = findViewById(R.id.imageListBackButton);
        buttonBack.setOnClickListener((View view) -> finish());

        Button buttonCamera = findViewById(R.id.imageListCameraButton);
        buttonCamera.setVisibility(View.GONE);

        progressBar = findViewById(R.id.progressBarPictureDisplayprogressBarPictureDisplay);
        activityView = findViewById(R.id.pictureListView);

        updateList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);

        // save reference to the menu
        this.menu = menu;
        this.menu.getItem(0).setVisible(false);
        this.menu.getItem(1).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                DeleteDialog deleteDialog = new DeleteDialog();
                deleteDialog.show(getSupportFragmentManager(), "deletePictureFromList");
                break;
            default:
                break;
        }
        return false;
    }

    private void updateList() {
        PictureListFragment pictureListFragment = (PictureListFragment) getSupportFragmentManager().findFragmentById(R.id.imageListFragment);
        if (pictureListFragment != null) {
            showProgress(true);
            HTTPRequestSender httpRequestSender = new HTTPRequestSender(this);
            httpRequestSender.getImages(AccountManager.getAccount().getEmail());
        }
    }

    private void deleteImages(Uri[] uris) {
        String email = AccountManager.getAccount().getEmail();
        String[] paths = new String[uris.length];
        for (int i = 0; i < uris.length; i++) {
            String path = uris[i].getPath();
            path = path.substring(path.indexOf('/', path.indexOf('/', path.indexOf('/') + 1) + 1) + 1);
            paths[i] = path;
        }

        HTTPRequestSender httpRequestSender = new HTTPRequestSender(this);
        httpRequestSender.deleteImages(email, paths);
    }

    @Override
    public void onBackPressed() {
        PictureListFragment pictureListFragment = (PictureListFragment) getSupportFragmentManager().findFragmentById(R.id.imageListFragment);
        if (pictureListFragment == null || !pictureListFragment.exitSelectMode()) {
            // if the list is not in select mode
            super.onBackPressed();
        }
    }

    @Override
    public void onItemClick(int index) {
        // move to ServerPictureDisplayActivity
        Intent i = new Intent(this, ServerPictureDisplayActivity.class);
        i.putExtra("currIndex", index);
        startActivity(i);
        finish();
    }

    @Override
    public void onEnterBrowseMode() {
        // hide delete button on the menu
        this.menu.getItem(1).setVisible(false);
    }

    @Override
    public void onEnterSelectMode() {
        // show delete button on the menu
        this.menu.getItem(1).setVisible(true);
    }

    @Override
    public void onClickDelete() {
        PictureListFragment pictureListFragment = (PictureListFragment) getSupportFragmentManager().findFragmentById(R.id.imageListFragment);

        if (pictureListFragment != null) {
            Uri[] selected = pictureListFragment.getSelectedUris();

            deleteImages(selected);
            pictureListFragment.deleteSelected();
            Toast.makeText(this, getString(R.string.toast_images_deleted), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onGetImagesResponse(JSONArray result) {
        PictureListFragment pictureListFragment = (PictureListFragment) getSupportFragmentManager().findFragmentById(R.id.imageListFragment);
        if (pictureListFragment != null && result != null) {
            ArrayList<Uri> imagesUris = new ArrayList<>();
            ArrayList<String> imagesNames = new ArrayList<>();

            try {
                for (int i = 0; i < result.length(); i++) {
                    JSONObject imageData = result.getJSONObject(i);
                    imagesNames.add(imageData.getString("name"));
                    imagesUris.add(Uri.parse(imageData.getString("path")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            pictureListFragment.setData(imagesUris, imagesNames);
            pictureListFragment.setListener(this);
        }
        showProgress(false);
    }
}
