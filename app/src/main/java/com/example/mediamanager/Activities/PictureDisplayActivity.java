package com.example.mediamanager.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.example.mediamanager.*;
import com.example.mediamanager.Dialogs.ConfirmUploadDialog;
import com.example.mediamanager.Dialogs.DeleteDialog;
import com.example.mediamanager.Dialogs.UploadDialog;
import com.example.mediamanager.Utils.*;
import com.google.api.client.http.ByteArrayContent;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Locale;

public class PictureDisplayActivity extends CameraCaptureActivity implements DeleteDialog.DeleteDialogListener, UploadDialog.UploadDialogListener, ConfirmUploadDialog.ConfirmUploadDialogListener, HTTPRequestSender.HTTPListener {

    private Menu menu;

    private ArrayList<Uri> imagesUris = new ArrayList<>();
    private ArrayList<String> imagesNames = new ArrayList<>();

    private int currIndex = 0;

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_display);

        checkStorageAndCameraPermissions();

        imageView = findViewById(R.id.imageDisplayImageView);

        ImageButton buttonLeft = findViewById(R.id.imageDisplayLeftButton);
        buttonLeft.setOnClickListener((View view) -> prevImage());

        ImageButton buttonRight = findViewById(R.id.imageDisplayRightButton);
        buttonRight.setOnClickListener((View view) -> nextImage());

        Button buttonBack = findViewById(R.id.imageDisplayBackButton);
        buttonBack.setOnClickListener((View view) -> onBackPressed());

        Button buttonCamera = findViewById(R.id.imageDisplayCameraButton);
        buttonCamera.setOnClickListener((View view) -> takePicture());

        if (savedInstanceState != null) {
            currIndex = savedInstanceState.getInt("currIndex");
        } else {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                currIndex = extras.getInt("currIndex");
            }
        }

        getImages();

        currIndex--;
        nextImage();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("currIndex", currIndex);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);

        // save reference to the menu
        this.menu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                DeleteDialog dialog = new DeleteDialog();
                dialog.show(getSupportFragmentManager(), "deletePictureFromDisplay");
                break;
            case R.id.action_upload:
                UploadDialog uploadDialog = new UploadDialog();
                uploadDialog.show(getSupportFragmentManager(), "uploadPictureFromList");
                break;
            default:
                break;
        }
        return false;
    }

    private void nextImage() {
        // increase currIndex
        if (currIndex < imagesUris.size() - 1) {
            currIndex++;
        } else {
            currIndex = 0;
        }

        updateImage();
        updateTexts();
    }

    private void prevImage() {
        // reduce currIndex
        if (currIndex > 0) {
            currIndex--;
        } else {
            currIndex = imagesUris.size() - 1;
        }

        updateImage();
        updateTexts();
    }

    private void updateImage() {
        try {
            Picasso.get()
                    .load(imagesUris.get((currIndex)))
                    .fit()
                    .centerInside()
                    .into(imageView);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    private void updateTexts() {
        TextView textViewName = findViewById(R.id.imageDisplayName);
        TextView textViewIndex = findViewById(R.id.imageDisplayIndex);
        try {
            // update name text field
            textViewName.setText(imagesNames.get(currIndex));

            // update page text field
            String baseIndexText = "%d/%d";
            textViewIndex.setText(String.format(Locale.getDefault(), baseIndexText, currIndex + 1, imagesUris.size()));
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            textViewName.setText("");
            textViewIndex.setText(String.format(Locale.getDefault(), "0/0", currIndex + 1, imagesUris.size()));
        }
    }

    private float[] getImageSize() {
        Guideline gTop = findViewById(R.id.imageDisplayGuidelineTop);
        if (gTop != null) {
            // horizontal guideline exists -> landscape mode

            Guideline gBottom = findViewById(R.id.imageDisplayGuidelineBottom);

            float percentTop = ((ConstraintLayout.LayoutParams) gTop.getLayoutParams()).guidePercent;
            float percentBottom = ((ConstraintLayout.LayoutParams) gBottom.getLayoutParams()).guidePercent;

            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            int screenHeight = size.y;

            return new float[]{0, screenHeight * percentBottom - screenHeight * percentTop};
        } else {
            // horizontal guideline doesn't exist -> portrait mode

            Guideline gLeft = findViewById(R.id.imageDisplayGuidelineLeft);
            Guideline gRight = findViewById(R.id.imageDisplayGuidelineRight);

            float percentLeft = ((ConstraintLayout.LayoutParams) gLeft.getLayoutParams()).guidePercent;
            float percentRight = ((ConstraintLayout.LayoutParams) gRight.getLayoutParams()).guidePercent;

            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            int screenWidth = size.x;

            return new float[]{screenWidth * percentRight - screenWidth * percentLeft, 0};
        }
    }

    private void getImages() {
        try {
            this.imagesUris = new ArrayList<>();
            this.imagesNames = new ArrayList<>();

            // get images data JSON
            String imagesString = MediaManager.getImages();
            JSONArray imagesJSON = new JSONArray(imagesString);

            for (int i = imagesJSON.length() - 1; i >= 0; i--) {
                JSONObject imageJSON = imagesJSON.getJSONObject(i);

                // extract data into lists
                Uri uri = Uri.parse(imageJSON.getString("uri"));
                String name = imageJSON.getString("name");

                imagesUris.add(uri);
                imagesNames.add(name);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void pictureTaken() {
        super.pictureTaken();

        recreate();
    }

    private void checkStorageAndCameraPermissions() {
        // if the r/w external storage permission is not granted, exit the activity
        if (!PermissionManager.checkStorageAndCameraPermissions(this)) {
            finish();
        }
    }

    private boolean checkDrivePermission() {
        return PermissionManager.checkDrivePermission(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, PictureListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PermissionManager.GOOGLE_SIGN_IN_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (checkDrivePermission()) {
                    uploadToDrive();
                }
            }
        } else if (requestCode == PermissionManager.DRIVE_PERMISSION_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                uploadToDrive();
            }
        }
    }

    @Override
    public void onClickDelete() {
        if (MediaManager.deleteImage(imagesUris.get(currIndex)) != -1) {
            this.imagesUris.remove(currIndex);
            this.imagesNames.remove(currIndex);
            Toast.makeText(this, getString(R.string.toast_image_deleted), Toast.LENGTH_SHORT).show();
            currIndex--;
            nextImage();
        }
    }

    @Override
    public void onClickUploadServer() {
        ConfirmUploadDialog confirmUploadDialog = new ConfirmUploadDialog();
        confirmUploadDialog.setDestination(ConfirmUploadDialog.SERVER);
        confirmUploadDialog.show(getSupportFragmentManager(), "uploadPictureToServer");
    }

    @Override
    public void onClickUploadDrive() {
        ConfirmUploadDialog confirmUploadDialog = new ConfirmUploadDialog();
        confirmUploadDialog.setDestination(ConfirmUploadDialog.DRIVE);
        confirmUploadDialog.show(getSupportFragmentManager(), "uploadPictureToDrive");

    }

    @Override
    public void onConfirmUploadServer() {
        try {
            Uri imageUri = this.imagesUris.get(this.currIndex);
            String imageName = this.imagesNames.get(this.currIndex);
            String userEmail = AccountManager.getAccount().getEmail();

            HTTPRequestSender requestSender = new HTTPRequestSender(this);
            requestSender.uploadImage(userEmail, imageUri, imageName, AccountManager.getFirebaseToken());
        } catch (NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.toast_error_upload_to_server), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConfirmUploadDrive() {
        if (PermissionManager.checkGoogleAccount(this)) {
            if (checkDrivePermission()) {
                uploadToDrive();
            }
        }
    }

    private void uploadToDrive() {
        Uri imgUri = imagesUris.get(currIndex);
        String imgName = imagesNames.get(currIndex);

        new GenericAsyncTask(() -> {

            String type = MediaManagerApplication.getAppContext().getContentResolver().getType(imgUri);
            DriveServiceHelper dsh = AccountManager.getDriveServiceHelper();
            Bitmap image = MediaManager.getBitmap(imgUri);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 60, stream);
            byte[] byteArray = stream.toByteArray();
            try {
                dsh.createFile(imgName, type, new ByteArrayContent(type, byteArray))
                        .addOnSuccessListener((pFileId) -> {
                            if (pFileId != null) {
                                Toast.makeText(MediaManagerApplication.getAppContext(), getString(R.string.toast_image_uploaded_to_drive), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(MediaManagerApplication.getAppContext(), getString(R.string.toast_error_upload_to_drive), Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(e -> e.printStackTrace());
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }).execute();
    }

    @Override
    public void onUploadImageResponse(String result) {
        if (result != null && !result.isEmpty()) {
            Toast.makeText(this, getString(R.string.toast_image_uploaded_to_server), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.toast_error_upload_to_server), Toast.LENGTH_SHORT).show();
        }
    }
}
