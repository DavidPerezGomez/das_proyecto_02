package com.example.mediamanager.Activities;

import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.mediamanager.*;
import com.example.mediamanager.Dialogs.DeleteDialog;
import com.example.mediamanager.Utils.AccountManager;
import com.example.mediamanager.Utils.HTTPRequestSender;
import com.example.mediamanager.Utils.UnsafeOkHttpClient;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class ServerPictureDisplayActivity extends LoaderActivity implements DeleteDialog.DeleteDialogListener, HTTPRequestSender.HTTPListener {

    private Menu menu;

    private ArrayList<Uri> imagesUris = new ArrayList<>();
    private ArrayList<String> imagesNames = new ArrayList<>();

    private int currIndex = 0;

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_display);

        imageView = findViewById(R.id.imageDisplayImageView);

        ImageButton buttonLeft = findViewById(R.id.imageDisplayLeftButton);
        buttonLeft.setOnClickListener((View view) -> prevImage());

        ImageButton buttonRight = findViewById(R.id.imageDisplayRightButton);
        buttonRight.setOnClickListener((View view) -> nextImage());

        Button buttonBack = findViewById(R.id.imageDisplayBackButton);
        buttonBack.setOnClickListener((View view) -> onBackPressed());

        Button buttonCamera = findViewById(R.id.imageDisplayCameraButton);
        buttonCamera.setVisibility(View.GONE);

        progressBar = findViewById(R.id.progressBarPictureDisplay);
        activityView = findViewById(R.id.pictureDisplayView);

        if (savedInstanceState != null) {
            currIndex = savedInstanceState.getInt("currIndex");
        } else {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                currIndex = extras.getInt("currIndex");
            }
        }

        getImages();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("currIndex", currIndex);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);

        // save reference to the menu
        this.menu = menu;
        this.menu.getItem(0).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                DeleteDialog dialog = new DeleteDialog();
                dialog.show(getSupportFragmentManager(), "deletePictureFromDisplay");
                break;
            default:
                break;
        }
        return false;
    }

    private void nextImage() {
        // increase currIndex
        if (currIndex < imagesUris.size() - 1) {
            currIndex++;
        } else {
            currIndex = 0;
        }

        updateImage();
        updateTexts();
    }

    private void prevImage() {
        // reduce currIndex
        if (currIndex > 0) {
            currIndex--;
        } else {
            currIndex = imagesUris.size() - 1;
        }

        updateImage();
        updateTexts();
    }

    private void updateImage() {
        try {
            Picasso.Builder builder = new Picasso.Builder(this);
            builder.downloader(new OkHttp3Downloader(UnsafeOkHttpClient.getUnsafeOkHttpClient()));
            Picasso picasso = builder.build();
            picasso
                    .load(imagesUris.get((currIndex)))
                    .fit()
                    .centerInside()
                    .into(imageView);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    private void updateTexts() {
        TextView textViewName = findViewById(R.id.imageDisplayName);
        TextView textViewIndex = findViewById(R.id.imageDisplayIndex);
        try {
            // update name text field
            textViewName.setText(imagesNames.get(currIndex));

            // update page text field
            String baseIndexText = "%d/%d";
            textViewIndex.setText(String.format(Locale.getDefault(), baseIndexText, currIndex + 1, imagesUris.size()));
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
            textViewName.setText("");
            textViewIndex.setText(String.format(Locale.getDefault(), "0/0", currIndex + 1, imagesUris.size()));
        }
    }

    private float[] getImageSize() {
        Guideline gTop = findViewById(R.id.imageDisplayGuidelineTop);
        if (gTop != null) {
            // horizontal guideline exists -> landscape mode

            Guideline gBottom = findViewById(R.id.imageDisplayGuidelineBottom);

            float percentTop = ((ConstraintLayout.LayoutParams) gTop.getLayoutParams()).guidePercent;
            float percentBottom = ((ConstraintLayout.LayoutParams) gBottom.getLayoutParams()).guidePercent;

            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            int screenHeight = size.y;

            return new float[]{0, screenHeight * percentBottom - screenHeight * percentTop};
        } else {
            // horizontal guideline doesn't exist -> portrait mode

            Guideline gLeft = findViewById(R.id.imageDisplayGuidelineLeft);
            Guideline gRight = findViewById(R.id.imageDisplayGuidelineRight);

            float percentLeft = ((ConstraintLayout.LayoutParams) gLeft.getLayoutParams()).guidePercent;
            float percentRight = ((ConstraintLayout.LayoutParams) gRight.getLayoutParams()).guidePercent;

            Point size = new Point();
            getWindowManager().getDefaultDisplay().getSize(size);
            int screenWidth = size.x;

            return new float[]{screenWidth * percentRight - screenWidth * percentLeft, 0};
        }
    }

    private void getImages() {
        showProgress(true);
        HTTPRequestSender httpRequestSender = new HTTPRequestSender(this);
        httpRequestSender.getImages(AccountManager.getAccount().getEmail());
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, ServerPictureListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClickDelete() {
        String email = AccountManager.getAccount().getEmail();
        String path = imagesUris.get(currIndex).getPath();
        path = path.substring(path.indexOf('/', path.indexOf('/', path.indexOf('/') + 1) + 1) + 1);

        HTTPRequestSender httpRequestSender = new HTTPRequestSender(this);
        httpRequestSender.deleteImage(email, path);

        // no se espera a respuesta
        // siempre se borra el objeto local
        this.imagesUris.remove(currIndex);
        this.imagesNames.remove(currIndex);
        currIndex--;
        nextImage();
    }

    @Override
    public void onGetImagesResponse(JSONArray result) {
        if (result != null) {
            this.imagesUris = new ArrayList<>();
            this.imagesNames = new ArrayList<>();

            try {
                for (int i = 0; i < result.length(); i++) {
                    JSONObject imageData = result.getJSONObject(i);
                    imagesNames.add(imageData.getString("name"));
                    imagesUris.add(Uri.parse(imageData.getString("path")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        showProgress(false);

        currIndex--;
        nextImage();
    }

}
