package com.example.mediamanager.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.example.mediamanager.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Abstract activity to handle taking pictures with the camera.
 */
public abstract class CameraCaptureActivity extends AppCompatActivity {

    private final int CODIGO_FOTO_ARCHIVO = 10;

    private Uri uriimagen;
    private File fichImg;

    public void takePicture() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String nombrefich = "IMG_" + timeStamp + "_";
        File externalStorage = Environment.getExternalStorageDirectory();
        File folder = new File(externalStorage, "MediaManager");
        if (!folder.exists() && !folder.isDirectory()) {
            folder.mkdirs();
        }
        uriimagen = null;
        try {
            fichImg = File.createTempFile(nombrefich, ".jpg", folder);
            uriimagen = FileProvider.getUriForFile(this, "com.example.mediamanager.provider", fichImg);
            Intent elIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            elIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriimagen);
            startActivityForResult(elIntent, CODIGO_FOTO_ARCHIVO);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pictureTaken() {
        if (fichImg != null) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(fichImg);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
        } else {
            Toast.makeText(this, getString(R.string.toast_camera_error), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CODIGO_FOTO_ARCHIVO && resultCode == RESULT_OK) {
            pictureTaken();
        }
    }


}
