package com.example.mediamanager.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import com.example.mediamanager.Utils.AccountManager;
import com.example.mediamanager.Utils.HTTPRequestSender;
import com.example.mediamanager.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class LogInActivity extends LoaderActivity implements HTTPRequestSender.HTTPListener {

    private EditText editTextEmail;
    private EditText editTextPassword;
    private Button buttonLogIn;
    private Button buttonMoveToSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String languageCode = preferences.getString("language", "es");
        Locale nuevaloc = new Locale(languageCode);
        Locale.setDefault(nuevaloc);
        Configuration config = new Configuration();
        config.locale = nuevaloc;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_log_in);

        AccountManager.signOut();

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextPassword.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin();
                return true;
            }
            return false;
        });

        buttonLogIn = findViewById(R.id.buttonLogIn);
        buttonLogIn.setOnClickListener(view -> attemptLogin());
        buttonMoveToSignIn = findViewById(R.id.buttonMoveToSignIn);
        buttonMoveToSignIn.setOnClickListener(view -> {
            Intent i = new Intent(this, RegisterActivity.class);
            startActivity(i);
            finish();
        });

        progressBar = findViewById(R.id.progressBarLogIn);
        activityView = findViewById(R.id.logInForm);
    }

    private void attemptLogin() {
        // Reset errors.
        editTextEmail.setError(null);
        editTextPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            editTextPassword.setError("Contraseña no válida");
            focusView = editTextPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError("Este campo no puede esta vacío");
            focusView = editTextEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            editTextEmail.setError("Email inválido");
            focusView = editTextEmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            editTextPassword.setText("");
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            HTTPRequestSender authTask = new HTTPRequestSender(this);
            authTask.logIn(email, password);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    @Override
    public void onLogInResponse(JSONObject result) {
        showProgress(false);
        if (result != null) {
            try {
                String email = result.getString("email");
                String name = result.getString("name");
                AccountManager.signIn(email, name);

                Intent i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            editTextPassword.setError("Contraseña incorrecta");
            editTextPassword.requestFocus();
        }
    }
}
