package com.example.mediamanager.Activities.Support;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mediamanager.MediaManagerApplication;
import com.example.mediamanager.R;
import com.example.mediamanager.Utils.UnsafeOkHttpClient;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.ArrayList;

public class PictureListAdapter extends GenericSelectionAdapter<PictureListViewHolder> implements PictureListViewHolder.PictureListViewHolderListener {

    private ArrayList<Uri> uris;
    private ArrayList<String> names;

    public PictureListAdapter(SelectionAdapterListener listener, ArrayList<Uri> uris, ArrayList<String> names) {
        super(listener);
        this.uris = uris;
        this.names = names;
    }

    @NonNull
    @Override
    public PictureListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemLayout = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new PictureListViewHolder(itemLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull PictureListViewHolder pictureListViewHolder, int i) {
        Picasso.Builder builder = new Picasso.Builder(MediaManagerApplication.getAppContext());
        builder.downloader(new OkHttp3Downloader(UnsafeOkHttpClient.getUnsafeOkHttpClient()));
        Picasso picasso = builder.build();
        RequestCreator requestCreator = picasso.load(uris.get(i));
        requestCreator
                .fit()
                .centerInside()
                .into(pictureListViewHolder.imageView);

        pictureListViewHolder.textView.setText(names.get(i));

        // listen to user inputs
        pictureListViewHolder.setListener(this, i);

        // set selected if needed
        pictureListViewHolder.itemView.setSelected(this.getSelected().contains(i));
    }


    @Override
    public int getItemCount() {
        return uris.size();
    }

    /**
     * @return A list containing all the uris selected from the list.
     */
    public Uri[] getSelectedUris() {
        Uri[] uris = new Uri[this.getSelected().size()];
        int pos = 0;
        for (int index : this.getSelected()) {
            uris[pos] = this.uris.get(index);
            pos++;
        }

        return uris;
    }

    /**
     * Removes the selected items from the list.
     */
    public void deleteSelectedItems() {
        // sort selected list from biggest to smallest
        this.getSelected().sort((o1, o2) -> o2 - o1);

        // iterate over selected items
        for (int index : this.getSelected()) {
            if (this.uris.remove(index) != null) {
                this.names.remove(index);
                notifyItemRemoved(index);
            }
        }

        int first = this.getSelected().get(this.getSelected().size() - 1);
        notifyItemRangeChanged(first, this.uris.size() - first);

        exitSelectMode();
    }

    @Override
    public void onItemClick(int index) {
        super.onItemClick(index);
    }

    @Override
    public void onItemLongClick(int index) {
        super.onItemLongClick(index);
    }
}
