package com.example.mediamanager.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.mediamanager.*;
import com.example.mediamanager.Activities.Support.GenericListFragment;
import com.example.mediamanager.Dialogs.DeleteDialog;
import com.example.mediamanager.Dialogs.UploadDialog;
import com.example.mediamanager.Utils.MediaManager;
import com.example.mediamanager.Utils.PermissionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PictureListActivity extends CameraCaptureActivity implements GenericListFragment.ListFragmentListener, DeleteDialog.DeleteDialogListener {

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_list);

        checkStorageAndCameraPermissions();

        Button buttonBack = findViewById(R.id.imageListBackButton);
        buttonBack.setOnClickListener((View view) -> finish());

        Button buttonCamera = findViewById(R.id.imageListCameraButton);
        buttonCamera.setOnClickListener((View view) -> takePicture());

        updateList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);

        // save reference to the menu
        this.menu = menu;
        this.menu.getItem(0).setVisible(false);
        this.menu.getItem(1).setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                DeleteDialog deleteDialog = new DeleteDialog();
                deleteDialog.show(getSupportFragmentManager(), "deletePictureFromList");
                break;
            case R.id.action_upload:
                UploadDialog uploadDialog = new UploadDialog();
                uploadDialog.show(getSupportFragmentManager(), "uploadPictureFromList");
                break;
            default:
                break;
        }
        return false;
    }

    private void updateList() {
        PictureListFragment pictureListFragment = (PictureListFragment) getSupportFragmentManager().findFragmentById(R.id.imageListFragment);
        if (pictureListFragment != null) {
            // get images from device and put them on the list
            ArrayList[] images = getImages();

            if (images != null) {
                ArrayList<Uri> imagesUris = (ArrayList<Uri>) images[0];
                ArrayList<String> imagesNames = (ArrayList<String>) images[1];

                pictureListFragment.setData(imagesUris, imagesNames);
                pictureListFragment.setListener(this);
            }
        }
    }

    private ArrayList[] getImages() {
        try {
            ArrayList<Uri> imagesUris = new ArrayList<>();
            ArrayList<String> imagesNames = new ArrayList<>();

            // get images data JSON
            String imagesString = MediaManager.getImages();
            JSONArray imagesJSON = new JSONArray(imagesString);

            for (int i = imagesJSON.length() - 1; i >= 0; i--) {
                JSONObject imageJSON = imagesJSON.getJSONObject(i);

                // extract data into lists
                Uri uri = Uri.parse(imageJSON.getString("uri"));
                String name = imageJSON.getString("name");

                imagesUris.add(uri);
                imagesNames.add(name);
            }

            return new ArrayList[]{imagesUris, imagesNames};
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean deleteImages(Uri[] uris) {
        try {
            int cols = MediaManager.deleteImages(uris);
            return cols > 0;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void pictureTaken() {
        super.pictureTaken();

        recreate();
    }

    private void checkStorageAndCameraPermissions() {
        // if the r/w external storage permission is not granted, exit the activity
        if (!PermissionManager.checkStorageAndCameraPermissions(this)) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        PictureListFragment pictureListFragment = (PictureListFragment) getSupportFragmentManager().findFragmentById(R.id.imageListFragment);
        if (pictureListFragment == null || !pictureListFragment.exitSelectMode()) {
            // if the list is not in select mode
            super.onBackPressed();
        }
    }

    @Override
    public void onItemClick(int index) {
        // move to PictureDisplayActivity
        Intent i = new Intent(this, PictureDisplayActivity.class);
        i.putExtra("currIndex", index);
        startActivity(i);
        finish();
    }

    @Override
    public void onEnterBrowseMode() {
        // hide delete button on the menu
        this.menu.getItem(1).setVisible(false);
    }

    @Override
    public void onEnterSelectMode() {
        // show delete button on the menu
        this.menu.getItem(1).setVisible(true);
    }

    @Override
    public void onClickDelete() {
        PictureListFragment pictureListFragment = (PictureListFragment) getSupportFragmentManager().findFragmentById(R.id.imageListFragment);

        if (pictureListFragment != null) {
            Uri[] selected = pictureListFragment.getSelectedUris();

            if (deleteImages(selected)) {
                pictureListFragment.deleteSelected();
                Toast.makeText(this, getString(R.string.toast_images_deleted), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
