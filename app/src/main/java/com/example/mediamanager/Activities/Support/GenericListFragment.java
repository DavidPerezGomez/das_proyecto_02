package com.example.mediamanager.Activities.Support;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.mediamanager.Activities.PictureListFragment;
import com.example.mediamanager.R;

public abstract class GenericListFragment<T extends GenericSelectionAdapter> extends Fragment implements GenericSelectionAdapter.SelectionAdapterListener {

    public interface ListFragmentListener {

        default void onItemClick(int index) {}

        default void onEnterBrowseMode() {}

        default void onEnterSelectMode() {}
    }

    private PictureListFragment.ListFragmentListener listener;
    public RecyclerView list;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.list = getActivity().findViewById(R.id.recyclerView);
    }

    public void setData() {
        this.list = getActivity().findViewById(R.id.recyclerView);
    }

    /**
     * Set the listener that will be notified of the user's actions on the list.
     *
     * @param listener Listener implementing ListFragmentListener.
     */
    public void setListener(PictureListFragment.ListFragmentListener listener) {
        this.listener = listener;
    }

    /**
     * Exit the list select mode and go back to the browse mode.
     *
     * @return False if the list was not in select mode, True otherwise.
     */
    public boolean exitSelectMode() {
        try {
            return ((T) this.list.getAdapter()).exitSelectMode();
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public void onItemClick(int index) {
        if (this.listener != null) {
            this.listener.onItemClick(index);
        }
    }

    @Override
    public void onEnterBrowseMode() {
        if (this.listener != null) {
            this.listener.onEnterBrowseMode();
        }
    }

    @Override
    public void onEnterSelectMode() {
        if (this.listener != null) {
            this.listener.onEnterSelectMode();
        }
    }

}
