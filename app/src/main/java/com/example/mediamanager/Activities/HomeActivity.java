package com.example.mediamanager.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.example.mediamanager.Utils.AccountManager;
import com.example.mediamanager.Dialogs.GoogleSignInFailDialog;
import com.example.mediamanager.Utils.PermissionManager;
import com.example.mediamanager.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

public class HomeActivity extends AppCompatActivity implements GoogleSignInFailDialog.GoogleSignInFailDialogListener {

    Intent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        pendingIntent = null;

        // verify that the user is logged in
        checkAccount();
        PermissionManager.signInFirebase();

        // try to log in with Google
        PermissionManager.checkGoogleAccount(this);

        Button buttonToPictures = findViewById(R.id.buttonToPictures);
        buttonToPictures.setOnClickListener((View view) -> {
            Intent intent = new Intent(this, PictureListActivity.class);
            if (PermissionManager.checkStorageAndCameraPermissions(this)) {
                startActivity(intent);
            } else {
                pendingIntent = intent;
            }
        });

        Button buttonToServerPictures = findViewById(R.id.buttonToServerPictures);
        buttonToServerPictures.setOnClickListener((View view) -> {
            Intent intent = new Intent(this, ServerPictureListActivity.class);
            startActivity(intent);
        });

        Button buttonSignOutGoogle = findViewById(R.id.buttonSignOutGoogle);
        buttonSignOutGoogle.setOnClickListener((View view) -> {
            AccountManager.signOutGoogle();
            PermissionManager.checkGoogleAccount(this);
        });
    }

    private void checkAccount() {
        if (AccountManager.getAccount() == null) {
            onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PermissionManager.STORAGE_AND_CAMERA_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // if the camera and storage permissions have been granted, continue with the intent
                    if (pendingIntent != null) {
                        startActivity(pendingIntent);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PermissionManager.GOOGLE_SIGN_IN_CODE) {
            Task<GoogleSignInAccount> task =
                    GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                task.getResult(ApiException.class);
                GoogleSignInAccount account = task.getResult();
                AccountManager.signInGoogle(account);
                Toast.makeText(this, getString(R.string.toast_google_login), Toast.LENGTH_SHORT).show();
            } catch (ApiException e) {
                e.printStackTrace();
                GoogleSignInFailDialog failDialog = new GoogleSignInFailDialog();
                failDialog.show(getSupportFragmentManager(), "retryGoogleSignIn");
            }
        }
    }

    @Override
    public void onClickRetrySignIn() {
        PermissionManager.checkGoogleAccount(this);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, LogInActivity.class);
        startActivity(i);
        finish();
    }
}
