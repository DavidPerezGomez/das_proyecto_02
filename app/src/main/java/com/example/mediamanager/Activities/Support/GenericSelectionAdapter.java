package com.example.mediamanager.Activities.Support;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Generic RecyclerView Adapter that handles user input and selection
 * and deselection of multiple items.
 * @param <T> Class of the RecyclerView ViewHolder to use.
 */
public abstract class GenericSelectionAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    public interface SelectionAdapterListener {

        default void onItemClick(int index) {
        }

        default void onItemLongClick(int index) {
        }

        default void onItemSelect(int index, ArrayList<Integer> selected) {}

        default void onItemDeselect(int index, ArrayList<Integer> selected) {}

        default void onEnterBrowseMode() {}

        default void onEnterSelectMode() {}
    }

    private final String BROWSE_MODE = "BROWSE_MODE";
    private final String SELECT_MODE = "SELECT_MODE";

    private String mode = BROWSE_MODE;

    // list of selected items
    private ArrayList<Integer> selected = new ArrayList<>();

    private SelectionAdapterListener listener;

    public GenericSelectionAdapter(SelectionAdapterListener listener) {
        this.listener = listener;
    }

    public SelectionAdapterListener getListener() {
        return listener;
    }

    @NonNull
    @Override
    public T onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull T viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public ArrayList<Integer> getSelected() {
        return this.selected;
    }

    /**
     * Exit the list select mode and go back to the browse mode.
     *
     * @return False if the list was not in select mode, True otherwise.
     */
    public boolean exitSelectMode() {
        if (this.mode.equals(SELECT_MODE)) {
            toBroseMode();
            return true;
        } else {
            return false;
        }
    }

    private void toBroseMode() {
        if (!this.mode.equals(BROWSE_MODE)) {
            // reset selected items
            this.selected.forEach(this::notifyItemChanged);
            this.selected = new ArrayList<>();

            // change mode
            this.mode = BROWSE_MODE;

            // notify listener
            if (this.listener != null) {
                this.listener.onEnterBrowseMode();
            }
        }
    }

    private void toSelectMode() {
        if (!this.mode.equals(SELECT_MODE)) {
            // reset selected items
            this.selected = new ArrayList<>();

            // change mode
            this.mode = SELECT_MODE;

            // notify listener
            if (this.listener != null) {
                this.listener.onEnterSelectMode();
            }
        }
    }

    /**
     * Add an item to the selected list or remove it from said list
     * if it was already in it.
     * @param index Index of the item to add or remove.
     *
     * @return Position in the selected list in which the item has been
     * inserted into. -1 if the item has been removed.
     */
    private int selectItem(int index) {
        if (this.mode.equals(SELECT_MODE)) {
            int indexInList = this.selected.indexOf(index);
            if (indexInList != -1) {
                // already selected

                // remove from selected list
                this.selected.remove(indexInList);
                notifyItemChanged(index);

                // notify listener
                if (this.listener != null) {
                    this.listener.onItemDeselect(index, this.selected);

                }

                // if no selected items, go to browse mode
                if (this.selected.isEmpty()) {
                    toBroseMode();
                }

                return -1;
            } else {
                // not selected

                // add to selected list
                this.selected.add(index);
                notifyItemChanged(index);

                // notify listener
                if (this.listener != null) {
                    this.listener.onItemSelect(index, this.selected);

                }

                return this.selected.size() - 1;
            }
        }

        return -1;
    }

    public void onItemClick(int index) {
        switch (this.mode) {
            default:
            case BROWSE_MODE:
                if (this.listener != null) {
                    this.listener.onItemClick(index);
                }
                break;
            case SELECT_MODE:
                selectItem(index);
                break;
        }
    }

    public void onItemLongClick(int index) {
        switch (this.mode) {
            default:
            case BROWSE_MODE:
                if (this.listener != null) {
                    this.listener.onItemLongClick(index);
                }
                toSelectMode();
            case SELECT_MODE:
                selectItem(index);
                break;
        }
    }
}
