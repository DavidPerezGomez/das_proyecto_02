package com.example.mediamanager.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import com.example.mediamanager.Utils.AccountManager;
import com.example.mediamanager.Utils.HTTPRequestSender;
import com.example.mediamanager.R;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends LoaderActivity implements HTTPRequestSender.HTTPListener {

    private EditText editTextEmail;
    private EditText editTextName;
    private EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        AccountManager.signOut();

        editTextEmail = findViewById(R.id.editTextRegisterEmail);
        editTextName = findViewById(R.id.editTextRegisterName);

        editTextPassword = findViewById(R.id.editTextRegisterPassword);
        editTextPassword.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptRegister();
                return true;
            }
            return false;
        });

        Button buttonRegister = findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(view -> attemptRegister());

        Button buttonBack = findViewById(R.id.buttonRegisterBack);
        buttonBack.setOnClickListener(view -> onBackPressed());

        progressBar = findViewById(R.id.progressBarRegister);
        activityView = findViewById(R.id.registerForm);
    }

    private void attemptRegister() {
        // Reset errors.
        editTextEmail.setError(null);
        editTextPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = editTextEmail.getText().toString();
        String name = editTextName.getText().toString();
        String password = editTextPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            editTextPassword.setError(getString(R.string.error_invalid_password));
            focusView = editTextPassword;
            cancel = true;
        }

        // Check for a valid name.
        if (TextUtils.isEmpty(name)) {
            editTextName.setError(getString(R.string.error_empty_field));
            focusView = editTextName;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            editTextEmail.setError(getString(R.string.error_empty_field));
            focusView = editTextEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            editTextEmail.setError(getString(R.string.error_invalid_email));
            focusView = editTextEmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            showProgress(true);
            HTTPRequestSender httpRequestSender = new HTTPRequestSender(this);
            httpRequestSender.register(email.toLowerCase(), name, password);
        }

    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }


    @Override
    public void onRegisterResponse(JSONObject result) {
        showProgress(false);
        if (result != null) {
            try {
                String email = result.getString("email");
                String name = result.getString("name");
                AccountManager.signIn(email, name);

                Intent i = new Intent(this, HomeActivity.class);
                startActivity(i);
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            editTextEmail.setError(getString(R.string.error_invalid_email_already_used));
            editTextEmail.requestFocus();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(this, LogInActivity.class);
        startActivity(i);
        finish();
    }
}
